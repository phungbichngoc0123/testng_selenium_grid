package grid;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class TestChrome {
	
	public RemoteWebDriver driver = null;
	
	@Test 
	public void setUp() throws MalformedURLException, InterruptedException {
		
		DesiredCapabilities caps = DesiredCapabilities.chrome();
		driver = new RemoteWebDriver(new URL("http://localhost:5587/wd/hub"), caps);
		
		//Maximuze the browser window
		driver.manage().window().maximize();
		
		//access url in node
		driver.get("http://amazon.com");
		Thread.sleep(10000);
		driver.close();
		

	}
	
}
