package grid;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

public class PhanTomJS {
	
	@Test
	public void test() throws InterruptedException, IOException {
		DesiredCapabilities caps = new DesiredCapabilities();
		//cho phep chup man hinh
		caps.setCapability("takeScressnshot", true);
		
		caps.setJavascriptEnabled(true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "D:\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		
		// webdriver
		WebDriver driver = new PhantomJSDriver(caps);
		
		
		driver.get("https://google.com");
		Thread.sleep(3000);
		
		File sourceFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(sourceFile, new File("D:\\Screenshot\\screenshot.png"), true);
	}

}
