package grid;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class GoogleTest {

  @Test
  @Parameters({"platform", "browser"})
  public void setUpTest(String platform, String browser) throws MalformedURLException, InterruptedException {
	  DesiredCapabilities caps = new DesiredCapabilities();
		RemoteWebDriver driver = null;
		// Platform
		if (platform.equalsIgnoreCase("Windows")) {
			caps.setPlatform(Platform.WINDOWS);
		}
		if (platform.equalsIgnoreCase("MAC")) {
			caps.setPlatform(Platform.MAC);
		}
		// Browser
		if (browser.equalsIgnoreCase("Internet Explorer")) {
			caps = DesiredCapabilities.internetExplorer();
			driver = new RemoteWebDriver(new URL("http://localhost:5588/wd/hub"), caps);
		}
		if (browser.equalsIgnoreCase("Chrome")) {
			caps = DesiredCapabilities.chrome();	
			driver = new RemoteWebDriver(new URL("http://localhost:5587/wd/hub"), caps);
			
		}
		if (browser.equalsIgnoreCase("Firefox")) {
			
			caps= DesiredCapabilities.firefox();
			driver = new RemoteWebDriver(new URL("http://localhost:5589/wd/hub"), caps);
		}
		
		driver.manage().window().maximize();
		
		driver.get("http://google.com");
		
		Thread.sleep(5000);
		driver.close();

  }
}
